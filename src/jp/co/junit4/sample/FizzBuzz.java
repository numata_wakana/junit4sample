package jp.co.junit4.sample;

public class FizzBuzz {

	public static void main(String[] args) {
	 // 挙動の確認用に1〜100までの整数を引数にして「checkFizzBuzz」を呼び出す
        for(int i = 1; i <= 100; i++) {
            System.out.println(checkFizzBuzz(i));
        }
	}
	 /*
     * 引数numが3の倍数なら「Fizz」
     * 5の倍数なら「Buzz」
     * 3と5両方の倍数なら「FizzBuzz」
     * それ以外ならそのまま数字を戻り値として返す
     *
     * 引数 int
     * 戻り値 String
     */
    public static String checkFizzBuzz(int num) {

    	String num1 = "Fizz";
    	String num2 = "Buzz";
    	String num3 = "FizzBuzz";

    	if(((num % 3) == 0)&&((num % 5) != 0)) {
    		return (num1);

		}else if(((num % 5) == 0)&&((num % 3) != 0)) {
			return (num2);

		}else if(((num % 3) == 0 )&&((num % 5) == 0)) {
			return (num3);

		}else {
		return Integer.toString(num);
		}
    }
}
