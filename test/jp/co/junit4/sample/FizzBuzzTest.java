/**
 *
 */
package jp.co.junit4.sample;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author numata.wakana
 *
 */

public class FizzBuzzTest {

	@Test
	public void testFizz() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	@Test
	public void testBuzz() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	@Test
	public void testFizzBuzz() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	@Test
	public void testSample1() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	@Test
	public void testSample2() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}
